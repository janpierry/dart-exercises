import 'dart:io';

import 'dart:math';

void main() {
  print('Escolha o exercício para testar: ');
  var _exercise = stdin.readLineSync();
  switch (int.parse(_exercise!)) {
    case 1:
      exerciseOne();
      break;
    case 2:
      exerciseTwo();
      break;
    case 3:
      exerciseThree();
      break;
    case 4:
      exerciseFour();
      break;
    case 5:
      exerciseFive();
      break;
    case 6:
      exerciseSix();
      break;
  }
}

void exerciseOne() {
  final _grades = [8, 9, 7, 6];
  final _sum = _grades.reduce((total, element) => total + element);
  final _average = _sum / _grades.length;
  print('As notas são: $_grades');
  print('Média: $_average');
}

void exerciseTwo() {
  final _randomGenerator = Random();
  final _generatedNumber = _randomGenerator.nextInt(100) + 1;
  var _input;
  var _parsedValue;
  stdout.writeln('Informe um número');
  do {
    var _answer;
    _input = stdin.readLineSync();
    try {
      _parsedValue = int.parse(_input);
      if (_parsedValue > _generatedNumber)
        _answer = 'Menos';
      else if (_parsedValue < _generatedNumber)
        _answer = 'Mais';
      else
        _answer = 'Acertou!';
    } on FormatException {
      print('Insira um valor numérico');
    }
    stdout.writeln(_answer);
  } while (_parsedValue != _generatedNumber);
}

void exerciseThree() {
  final _numbers = [1, 2, 3, 4, 5, 6, 7, 8, 9];
  final _primes = <int>[];

  bool _isPrimeNumber(int number) {
    var _divisibleForCount = 0;
    for (var i = 1; i <= number; i++) {
      if (number % i == 0) _divisibleForCount++;
    }
    return _divisibleForCount == 2;
  }

  _primes.addAll(_numbers.where(_isPrimeNumber));
  print('Os números indicados foram: $_numbers');
  print('Entre eles, os primos são: $_primes');
}

void exerciseFour() {
  late final _phraseReversed;
  stdout.writeln('Informe a palavra');
  var _phrase = stdin.readLineSync();
  _phrase = _phrase!.replaceAll(' ', '');
  _phraseReversed = _phrase.split('').reversed.join();
  print('É palíndromo? ${_phrase == _phraseReversed}');
}

void exerciseFive() {
  stdout.writeln('Informe a palavra');
  var _phrase = stdin.readLineSync();
  final Iterable<String> _phraseReversed = _phrase!.split(' ').reversed;
  print(_phraseReversed
      .reduce((previousValue, element) => '$previousValue $element'));
}

void exerciseSix() {
  final _generated = Random().nextInt(2);
  stdout.writeln('Escolha sua opção:');
  stdout.writeln('Pedra = 0');
  stdout.writeln('Papel = 1');
  stdout.writeln('Tesoura = 2');
  int _choice = int.parse(stdin.readLineSync()!);
  print(_generated);
  final _choices = {
    0: 2,
    1: 0,
    2: 1,
  };
  _choice == _generated
      ? print("Empatou")
      : _choices[_choice] == _generated
          ? print('Você venceu')
          : print('Você perdeu');
}
